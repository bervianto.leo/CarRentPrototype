﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentPrototype
{
    interface IOrderable
    {
        int GetPrice();
        string GetFormattedPrice();
        void SetPrice(int price);
        void SetPrice(string formattedPrice);
        string GetFormattedTitle();
        string GetId();
        void GenerateId();
    }
}
