﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentPrototype
{
    interface IManager
    {
        void Add(IManagable item);
        void Add(IManagable item, int atIndex);
        IManagable RemoveItem(IManagable item);
        void RemoveItems(IManagable item);
        IManagable GetItemAtIndex(int index);
        IList<IManagable> GetItemList();
    }
}
