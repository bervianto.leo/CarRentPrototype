﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Xml;

namespace CarRentPrototype
{
    public enum ManagerMenu
    {
        CARS_MENU,
        RENTAL_ORDERS_MENU,
        INCOME_MENU,
        EXPENSES_MENU
    }

    /// <summary>
    /// Interaction logic for ManagerApp.xaml
    /// </summary>
    public partial class ManagerApp : Window
    {
        //Maintain what menu currently viewed
        private ManagerMenu managerMenu;
        //Maintain grid layout associated with each ManagerMenu
        private IDictionary<ManagerMenu, Grid> managerMenuDictionary;


        public ManagerApp()
        {
            InitializeComponent();
            InitializeProperty();
        }

        private void InitializeProperty()
        {
            managerMenuDictionary = new Dictionary<ManagerMenu, Grid>();
            managerMenuDictionary.Add(ManagerMenu.CARS_MENU, CarsLayout);
            managerMenuDictionary.Add(ManagerMenu.RENTAL_ORDERS_MENU, RentalOrderLayout);
            managerMenuDictionary.Add(ManagerMenu.INCOME_MENU, IncomeLayout);
            managerMenuDictionary.Add(ManagerMenu.EXPENSES_MENU, ExpensesLayout);
            
            managerMenu = ManagerMenu.CARS_MENU;
            RefreshLayout();
        }

        private void RefreshLayout()
        {
            foreach (KeyValuePair<ManagerMenu, Grid> element in managerMenuDictionary)
            {
                if (element.Key == managerMenu)
                {
                    element.Value.Visibility = Visibility.Visible;
                }
                else
                {
                    element.Value.Visibility = Visibility.Hidden;
                }
            }
        }

        private void ViewCarsMenu(object sender, RoutedEventArgs e)
        {
            managerMenu = ManagerMenu.CARS_MENU;
            RefreshLayout();
        }

        private void ViewRentalOrdersMenu(object sender, RoutedEventArgs e)
        {
            managerMenu = ManagerMenu.RENTAL_ORDERS_MENU;
            RefreshLayout();
        }

        private void ViewIncomeMenu(object sender, RoutedEventArgs e)
        {
            managerMenu = ManagerMenu.INCOME_MENU;
            RefreshLayout();
        }

        private void ViewExpensesMenu(object sender, RoutedEventArgs e)
        {
            managerMenu = ManagerMenu.EXPENSES_MENU;
            RefreshLayout();
        }

        private void MaintenanceButton_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null)
            {
                string id = btn.Uid; // Bisa gunain ini untuk update data
                this.CarOnMaintenance.Text = "In Maintenance";
                bool enabled = btn.IsEnabled;
                if (enabled)
                {
                    btn.IsEnabled = false;
                    btn.Content = "In Maintenance";
                }
            }
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null)
            {
                string id = btn.Uid;
                if (btn.IsEnabled)
                {
                    MessageBox.Show("Accepted!");
                    btn.IsEnabled = false;
                    btn.Content = "Accepted";
                }
            }
        }

        private void RejectButton_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null)
            {
                string id = btn.Uid;
                MessageBox.Show(btn.Parent.ToString());
                if (btn.IsEnabled)
                {
                    MessageBox.Show("Rejected!");
                    btn.IsEnabled = false;
                    btn.Content = "Rejected";
                }
            }
        }
    }
}
